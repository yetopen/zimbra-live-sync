# Zimbra live sync scripts

This is the full set of scripts and configuration files as taken from [Zimbra's wiki 'Live Sync'](https://wiki.zimbra.com/wiki/Server_Live_sync) page.

## Introduction

This is an experimental solution to providing near-live synchronisation between two Zimbra servers so that one of them is live and the other is kept in a warm or very warm standby state.

The system is symmetrical. The sync can work in reverse when the mirror server becomes the active server. This allows easy fall-back to the original server once the failover condition is resolved.

Zimbra employs several different databases to store messages, message indexes, meta-data, account information
and configuration. Although it is possible to synchronise two Zimbra servers at the disk level using DRBD
or VSphere, the amount of disk operations from all these databases that need to be replicated would probably
take up a lot of bandwidth which may be debilitating and/or expensive to implement if the two servers
are in remote locations.

Fortunately, Zimbra keeps a log of almost all it's transactions in the **redolog**. The only thing not logged
here are changes to the LDAP database. An incremental backup is made up of an LDAP dump and a collection of
redologs. An incremental backup can be used to bring a backup server up to date if the if the last
full backup of the backup server was more recent than the oldest log in the redolog.

### Redolog

If the redolog can be piped to a mirror server in real time then all the mirror server has to do is keep
replaying the logs every so often and it will keep the same state as the live server. The only other thing
to keep up to date is the LDAP database. Fortunately, the LDAP database doesn't change that often so it
is quite easy to keep it synced on a directory level.

The easiest way to transfer the redologs is to use rsync. The only problem with that is that rsync does
not run continuously. It also won't handle the archiving of redo.log very efficiently. When redo.log is renamed
and moved to the archive rsync will delete it at the remote location then transfer it all over again to its
new location in the archive. If we can catch this move taking place then we can move and rename the file on
the mirror server before running rsync. Then rsync has very little to do, in theory, nothing except delete any
files that have been purged. Another issue with rsync is that the file may be in the process of being written
to when it is copied. This results in an incomplete file at the mirror. However, redologs are only ever appended
to and so only the last record will be corrupted. Zimbra is designed to be tolerant of redolog corruption
otherwise it would be of limited use as a disaster recovery tool.

To keep the redolog live, the `tail -f` command is used over ssh to pipe the file to the mirror. By calling
`tail -f -c +0` it tails right back to the zeroth byte of the file, effectively a *copy-then-stream* command.

### Redolog purging

If a Network edition is detected and incremental backups are enabled then the redologs are replayed before
any rsync is performed as well as after. This ensures that everything is replayed before the files all
disappear to the backup directory.

For the Open Source Edition, or Network Edition with no incremental backups scheduled, the redologs are purged
if they are more than a day old and have been replayed. If the mirror server is down then the redologs will
just accumulate on the live server to be replayed when the sync process is restarted before being purged.

### LDAP

LDAP stores it's data in `/opt/zimbra/data/ldap`. This can be copied using rsync to the mirror as long as
no changes take place during the copy. On versions of Zimbra older than 8.0, the directory is monitored for
this during the rsync operation and repeated if there was any change during that time. For Zimbra 8+ the
directory is monitored as before but changes are transfered using an LDIF export and import. This is
necessitated by the long time it takes to rsync the sparse files that LDAP now uses to store data.
Yes, it would be neater for old versions to use the LDIF method too but it wasn't broke so I didn't risk
fixing it yet.

## Known issues

* If the connection breaks at the very moment that the live stream of the redo.log starts, before the tail command reaches the point where it is tailing instead of cataloguing the file, then some of the redo.log will not make it to the mirror resulting in some loss of transactions. Fortunately, this is only ever likely very just after the log has rolled over so the worst-case losses should be minimal.

* LDAP is only checked every ten minutes so some losses are possible if the connection breaks in that time. However, LDAP isn't expected to change very often unless something major like a batch account migration is taking place.

* If any redologs go missing, or can't be replayed successfully for any reason, then there will be gaps synced email events. Mail may go missing on the mirror server. Check the *live_sync* logs for any log sequence numbers that appear not to have been transferred and processed. In the case of suspected data loss, stop all services and repeat the initial rsync process.

## Setup

inotify tools is required. For RHEL/CentOS

```bash
yum install inotify-tools
```

while for Ubuntu

```bash
apt install inotify-tools
```

## Installation and configuration

### Installation

The easiest way to deploy the script is to clone this repo on the server (if you have git installed), so you can keep up with updates:

```bash
git clone https://gitlab.com/yetopen/zimbra-live-sync.git /opt/zimbra/live_sync
```

Otherwise you can manually download the files or the whole repo and copy the content to `/opt/zimbra/live_sync`.

Make links for the log rotation with:

```bash
ln -s /opt/zimbra/live_sync/logrotate.d/zimbra_live_sync /etc/logrotate.d/
```

### Script configuration

You should copy the existing [`live_sync-EXAMPLE.conf`](live_sync-EXAMPLE.conf) to `live_sync.conf` and change the IPs of the two servers inside. The sync script will figure out on which server it's running and act
accordingly.

### ssh configuration

If you already have ssh keys configured between the servers you can skip this step.
Otherwise if you don't, or prefer to have dedicated ssh keys just for running sync commands do the following:

```bash
cd /opt/zimbra/.ssh
ssh-keygen -b 4096 -f live_sync
echo "command=\"/opt/zimbra/live_sync/sync_commands\" $( cat live_sync.pub )">>authorized_keys
```

This will create a key named `live_sync` and add it to the authorized ssh keys, restricted to running `/opt/zimbra/live_sync/sync_commands` command.

### Enabling redo log

For the Network Edition, redo logs are already being created and are periodically moved to create incremental backups.

For the Open Source Edition redo logs archiving must be enabled.

To see the current redo log related settings type the following **as user zimbra**

```bash
zmprov gacf | grep "RedoLog"
```

To enable redo log rollover on the open source version, type:

```bash
zmprov mcf zimbraRedoLogDeleteOnRollover FALSE
zmprov mcf zimbraRedoLogEnabled TRUE
```

You may also want to make the redo log rotation more frequent to guarantee a filesystem consistent redo log on
the mirror server at least up to the last, say, thirty minutes.
The live-streamed redo.log may not be consistent although it is unlikely this will ever be a problem except
with the very last record in the log.

For example, to force rollover every half and hour, type:

```bash
zmprov mcf zimbraRedoLogRolloverFileSizeKB 1
zmprov mcf zimbraRedoLogRolloverMinFileAge 30
```

This will rollover if the size of the redo log is over 1KB after 30 mins, which is very likely unless the mail server is not sending or receiving any mail at all during this time.

You may want to reduce the zimbraRedoLogRolloverMinFileAge even further while setting and testing this script just so you don't have to wait too long to see stuff happening between the severs.

### Mirror server

The mirror server should ideally have the same operating system as the live server and must have exactly the **same version of Zimbra** installed.

The **hostname must also be exactly the same**.

We're going to copy the whole `/opt/zimbra` so the script, its configuration and Zimbra itself is copied to the remote second node.

Run a first hot sync (with running Zimbra). This is going to take time depending on disk speed and bandwidth
between the two servers, but won't affect services.

On the **mirror server** run:

```bash
systemctl disable zimbra
systemctl stop zimbra
rsync -aHz --force --include=".*" --delete --sparse LIVE_SERVER:/opt/zimbra/ /opt/zimbra/
```

Once it's done we need to make a **cold** rsync, that is stop Zimbra services on the live server
and run files copy. This is still going to take some time because of the big LDAP sparse file.

On the **live server** run:

```bash
systemctl stop zimbra
rsync -aHz --force --include=".*" --delete --sparse /opt/zimbra/ MIRROR_SERVER:/opt/zimbra/
systemctl start zimbra
```

Now make sure we have a running version on Zimbra on the **mirror server**. Run:

```bash
systemctl start zimbra
service zimbra status
systemctl stop zimbra
```

### Running the script

Finally it's time to run the syncronization script. Not only have we copied all the Zimbra data from the
live to mirror server, we have also copied the script and SSH keys. We should now be able to try running the 
script.

**The syncronization will take place on the spare/mirror server**. As Zimbra user:

```bash
cd /opt/zimbra/live_sync
./live_syncd start
```

You can check what's going on in the background by reading or *tail*ing `log/live_sync.log`.

### Autostart at boot

There are two scripts available, one for *init* and one for *systemd* (RHEL/CentOS7 or Ubuntu16).

The script has to be installed on both servers, but **must be set to start on the mirror server only**.

#### init

CentOS:

```bash
cp etc/zimbra_live_sync /etc/init.d/
chmod 755 /etc/init.d/zimbra_live_sync
chkconfig --add zimbra_live_sync
```

On the live server make sure it does not start on boot

```bash
chkconfig zimbra_live_sync off
```

On the mirror server start the script and not Zimbra

```bash
chkconfig zimbra off
chkconfig zimbra_live_sync on
```

Ubuntu:

```bash
cp etc/zimbra_live_sync /etc/init.d/
chmod 755 /etc/init.d/zimbra_live_sync
```

On the mirror server make sure to start the script and not Zimbra

```bash
update-rc.d -f zimbra remove
update-rc.d zimbra_live_sync defaults
```

#### systemd

```bash
cp etc/zimbra-live-sync.service /etc/systemd/system/
systemctl daemon-reload
```

on the live server:

```bash
systemctl disable zimbra-live-sync
```

on the mirror server:

```bash
systemctl enable zimbra-live-sync
systemctl disable zimbra
```

## How to use

### Failover

If the live server fails and you want to start Zimbra on the mirror server, just stop the sync script and
start the services with:

```bash
# init
/etc/init.d/zimbra_live_sync stop
/etc/init.d/zimbra start
# systemd
systemctl stop zimbra-live-sync
systemctl start zimbra
```

### Fallback

When the live server comes back online and you want to catch up the changes done on the mirror one, which
has been *live* so far, run the sync script and once done restore Zimbra services on the live one.

On the live:

```bash
su - zimbra
zmcontrol stop
cd live_sync
./live_syncd start
```

on the mirror server:

```bash
# init
/etc/init.d/zimbra stop
# systemd
systemctl stop zimbra
```

On the live:

```bash
./live_syncd stop
zmcontrol start
```

Wait few moments and then restart the `live_syncd` service on the mirror.

## Monitoring live sync status

The script generates useful status information in `/opt/zimbra/live_sync/status`. You can measure the time
since the last successful operation and raise an alert if it's too high.

The check must be performed on the **mirror** server, that is where the `live_syncd` process is running.

### Nagios

There's a script to be called in [nagios](nagios) subdir. This is usually to be copied in 
`/usr/lib/nagios/plugins/contrib/check_zimbra_live_sync` and added to `/etc/nagios/nrpe.cfg` with

```bash
command[check_zimbra_live_sync]=/usr/lib/nagios/plugins/contrib/check_zimbra_live_sync -w $ARG1$ -c $ARG2$
```

### Zabbix

Import the XML template in the [zabbix](zabbix) subdir and attach it to the mirror server host. It will
trigger an error if the sync is older than one hour.

## Author

All the scripts have been written by and are (c) [Simon Blandford](mailto:simon@onepointltd.com), Onepoint Consulting Limited.

The git repository has been published by [Lorenzo Milesi](mailto:maxxer@yetopen.it), [YetOpen](https://www.yetopen.it) S.r.l..

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
