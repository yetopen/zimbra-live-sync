# Changelog

## Version 3.0.0 2018-10-26

Despite not being such a big upgrade I've decided to bump to version 3.0.0, to give a fresh start.
I've contacted Simon and he won't have time to keep this updated as he's not working on Zimbra anymore.

1. Updated stuff for GitLab
1. Added systemd config unit
1. Added README with setup instructions
1. Added config option for additional ssh parameters

## Version 2.1.5 2016-05-18

1. egrep and fgrep is deprecated. Change to grep -E and grep -F where used.

## Version 2.1.4 2013-03-12

1. HSM detection needs ldap, mailbox and mysql. Now done once at start.

## Version 2.1.2 2013-03-12

1. Added check to ensure mailboxd is kept running if HSM is being used
2. Suspend ldap/mailbox updates while HSM job is running

## Version 2.1.1 2013-02-12

1. Change grep -o "[0-9]*" to grep -o "[0-9]+" for RHEL in sync_commands

## Version 2.1.0 2013-02-11

1. Compatible with Zimbra version 8
2. Kill hanging sync_commands process on live server
3. Rename LDAP_DIR constant to LDAP_TEMP_DIR
4. Make rsync of ldap database aware of sparse files
5. For Zimbra version 8 and above use ldif export/import instead of rsync

## Version 2.0.1 2013-19-12

1. Preserve mailboxd runing state to allow warmer standby and functioning HSM
2. Don't refuse to start because of stale pid files after a system reboot

## Version 2.0.0 2012-08-29

1. Started changes log :)
2. Constants in upper case, variables in lower case
3. Exclude ldap log changes from inotify watch
4. Nagios plug-in to monitor time since last successful operation
5. Redologs are archived once successfully replayed
6. Redologs are retrieved from incremental backups on Zimbra Network edition
7. Redologs are purged independently after specified time on both servers
8. Magic path names and numbers now defined in constants
9. Log reporting by level and with filtering
10. Script aware of the java redolog replay process
11. convertd runs during redolog replay if enabled so indexing will work
